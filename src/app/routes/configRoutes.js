const express = require('express')
 router = express.Router(),
 authenticate = require('../controllers/authController'),
 userControl = require('../controllers/userController'),
 auth = require('../middlewares/auth.js')

router.post('/authenticate', authenticate.register)
.post('/register', userControl.create)
.get('/report/users/weekdays', auth, userControl.getWeekDays)


module.exports = app => app.use('/', router)