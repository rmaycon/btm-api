const userControl = require("../controllers/userController");
const express = require("express");

const router = express.Router();
const auth = require("../middlewares/auth");
// router.use(auth);

// usando o middleware auth para verificar se o usuário esta registrado/logado na aplicação
router.get("/", auth, userControl.getAll)
    .get("/:userId", auth, userControl.getById)
    .post("/", userControl.create)
    .patch("/:userId", auth, userControl.update)
    .delete("/:userId", auth, userControl.delete)

    .post("/:userId/invitations/:invitationId", auth, userControl.addIdGroupInUsers)
    .delete("/:userId/invitations/:invitationId", auth, userControl.removeIdGroupInUsers)



module.exports = app => app.use("/users", router);

// => app.use("/authenticate", router);