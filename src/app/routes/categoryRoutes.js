const express = require("express");
const categoryControl = require("../controllers/categoryController")
const groupMiddle = require('../middlewares/groupMiddleware')
const router = express.Router();
const auth = require('../middlewares/auth')
router.use(auth)

router.get('/:groupId/categories/', groupMiddle.groupExists, categoryControl.getAll)
    .get('/:groupId/categories/:categoryId', groupMiddle.groupExists, categoryControl.getById)
    .post('/:groupId/categories/', groupMiddle.groupExists, categoryControl.create)
    .patch('/:groupId/categories/:categoryId', groupMiddle.groupExists, categoryControl.update)
    .delete('/:groupId/categories/:categoryId', groupMiddle.groupExists, categoryControl.remove)

module.exports = app => app.use("/groups", router);