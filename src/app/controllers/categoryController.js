const Category = require('../models/Category'),
    GroupControl = require('./groupController'),
    utils = require('./utils'),
    TaskControl = require('./taskController'),
    responses = require('./responses'),
    errros = require('../../config/errors.json')

exports.getAll = async (req, res, next) => {
    try {
        const query = Category.find({
            belongs_to: req.groupId
        }).populate(['belongs_to', 'tasks'])
        query.exec(async (error, categories) => {
            if (categories && categories.length !== 0) {
                console.log(categories);

                return res.send(
                    categories.map(category => {
                        return {
                            id: category.id,
                            name: category.name,
                            belongs_to: {
                                id: category.belongs_to.id,
                                name: category.belongs_to.name,
                            },
                            tasks: category.tasks.map(task => {
                                return {
                                    id: task.id,
                                    name: task.name,
                                    description: task.description
                                }
                            })
                        }
                    })
                )
                // return res.send(categories)
            } else
                return responses.sendError(res, errros.categories_not_found)
            // return res.status(404).send({
            //     error: "No categories found."
            // })
        })
        // return res.send()
    } catch (error) {
        return utils.ServerError(res)

    }
}

exports.getById = async (req, res, next) => {
    try {
        const query = Category.findOne({
            _id: req.params.categoryId,
            belongs_to: req.groupId
        }).populate(['belongs_to', 'tasks'])
        query.exec(async (error, category) => {
            if (category) {

                return res.send({
                    id: category.id,
                    name: category.name,
                    belongs_to: {
                        id: category.belongs_to.id,
                        name: category.belongs_to.name,
                    },
                    tasks: category.tasks.map(task => {
                        return {
                            id: task.id,
                            name: task.name,
                            description: task.description
                        }
                    })
                })
            } else {

                return responses.sendError(res, errros.category_not_found)
            }

            // return res.status(404).send({
            //     error: "Category not found."
            // })
        })
    } catch (error) {
        return responses.sendError(res, errros.internal_error)
        // return responses.sendError(res, errros.internal_error)

        // return utils.ServerError(res)
    }
    // return res.send()
}

exports.create = async (req, res, next) => {
    try {
        let {
            name,
            description
        } = req.body
        if (!name) {
            errros.fields_not_found.fields = ['name']
            return responses.sendError(res, errros.fields_not_found)
            // return res.status(428).send({
            //     error: "Name cannot be null."
            // })
        } else {
            description = description || ""
            console.log(req.groupId);

            Category.create({
                name,
                description,
                belongs_to: req.groupId
            }, async (error, category) => {
                if (category) {
                    await GroupControl.addIdCategoryInGroup(req.groupId, category._id)
                    // return res.send(category)
                    const query = Category.findById(category.id).populate(['belongs_to', 'tasks'])
                    query.exec((error, category) => {
                        return res.send({
                            id: category.id,
                            name: category.name,
                            belongs_to: {
                                id: category.belongs_to.id,
                                name: category.belongs_to.name,
                            },
                            tasks: category.tasks.map(task => {
                                return {
                                    id: task.id,
                                    name: task.name,
                                    description: task.description
                                }
                            })
                        })
                    })
                } else {
                    console.log(error);

                    throw error
                }
            })
        }
    } catch (error) {
        // console.log(error.code);
        return responses.sendError(res, errros.internal_error)


        // return utils.ServerError(res)
    }
}

exports.update = async (req, res, next) => {
    try {
        const tmp = await Category.findOne({
            belongs_to: req.groupId,
            _id: req.params.categoryId
        }).select('tasks')

        let {
            name,
            description
        } = req.body

        if (!name) {
            errros.fields_not_found.fields = ['name']
            return responses.sendError(res, errros.fields_not_found)

            // return res.status(428).send({
            //     error: "Name cannot be null."
            // })
        } else {

            const query = Category.findByIdAndUpdate({
                _id: req.params.categoryId,
                belongs_to: req.groupId
            }, {
                name,
                description
            }, {
                new: true,
            }, async (error, category) => {
                if (category) {
                    const cat = Category.findById(category._id).populate(['tasks', 'belongs_to'])
                    cat.exec(async (error, category) => {
                        if (category) {
                            return res.send({
                                id: category.id,
                                name: category.name,
                                belongs_to: {
                                    id: category.belongs_to.id,
                                    name: category.belongs_to.name,
                                },
                                tasks: category.tasks.map(task => {
                                    return {
                                        id: task.id,
                                        name: task.name,
                                        description: task.description
                                    }
                                })
                            })
                        } else throw error
                    })
                    // return res.send(category)
                } else {
                    return responses.sendError(res, errros.category_not_found)
                    // return res.status(404).send({
                    //     error: "category not found"
                    // })
                }
            })
            query.exec()
        }

    } catch (error) {
        console.log(error.name);
        return responses.sendError(res, errros.internal_error)

        // return utils.ServerError(res)
    }
}

exports.remove = async (req, res, next) => {
    try {
        // await
        const category = await Category.findOne({
            _id: req.params.categoryId,
            belongs_to: req.groupId
        })
        if (category) {
            Category.findOneAndRemove({
                _id: req.params.categoryId,
                belongs_to: req.groupId
            }, async (error) => {
                if (!error) {
                    await TaskControl.removeMore(category.tasks, category.id)
                    await GroupControl.removeIdCategoryInGroup(req.groupId, req.params.categoryId)
                    return res.send({
                        msg: "Category delete successfull."
                    })
                }
            })
        } else {
            return responses.sendError(res, errros.category_not_found)

            // return res.status(404).send({
            //     errror: "Category not found"
            // })
        }
    } catch (error) {
        return responses.sendError(res, errros.internal_error)
        // return utils.ServerError(res)
    }
}

exports.addIdTaskInCategory = async (categoryId, taskId) => {
    try {
        const category = await Category.findById(categoryId)
        if (!category.tasks.find(task => {
                return task === taskId
            })) {
            await category.tasks.push(taskId)
            let {
                name,
                description,
                tasks
            } = category
            await Category.findByIdAndUpdate(categoryId, {
                name,
                description,
                tasks
            })
        }
        return true

    } catch (error) {
        return false
    }
}

exports.removeIdTaskInCategory = async (categoryId, taskId) => {
    try {
        const category = await Category.findById(categoryId)
        category.tasks = category.tasks.filter(task => {
            return task.toString() !== taskId.toString()
        })
        let {
            name,
            description,
            tasks
        } = category
        await Category.findByIdAndUpdate(categoryId, {
            name,
            description,
            tasks
        })
        return true
    } catch (error) {
        return false
    }
}

exports.removeMore = async (listCategories, groupId) => {
    try {

        await Promise.all(listCategories.map(async cat => {
            try {
                console.log(cat);

                /* Antes deletar a categoria, sera excluidas toras a tarefas da mesma */
                await Category.findById(cat, async (error, category) => {
                    if (category) {
                        await TaskControl.removeMore(category.tasks, category.id)
                    }

                })
            } catch (error) {
                console.log('cat erro');
                console.log(error);
            }
            try {
                /* Deletando a categria */
                await Category.findOneAndRemove({
                    _id: cat,
                    belongs_to: groupId
                })
            } catch (error) {
                console.log('cat 2 erro');
                console.log(error);
            }
            return true
        }))


    } catch (error) {
        return error
    }
}