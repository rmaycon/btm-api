exports.sendError = async (res, data) => {
    res.header('Error-Code', data.code)
    res.header('Name', data.name)
    return res.status(data.statusCode).send(data)
}