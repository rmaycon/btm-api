const Note = require('../models/Note'),
    GroupControl = require('./groupController'),
    utils = require('./utils')

/* métodos privados */
const ownerVerify = (owner, user) => {
    return owner === user;
};
exports.getAll = async (req, res, next) => {
    try {
        const query = Note.find({
            belongs_to: req.groupId
        })
        query.exec((error, notes) => {
            if (notes) {
                return res.send(notes)
            } else {
                return res.send({
                    error: error
                })
            }
        })
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}
exports.getById = async (req, res, next) => {
    try {
        const query = Note.findOne({
            _id: req.params.noteId,
            belongs_to: req.groupId
        })
        console.clear()
        await Note.findOne({
            _id: req.params.noteId
        }, (error, note) => {
            console.log(note);

        })

        query.exec((error, note) => {
            if (note) {
                return res.send(note)
            } else {
                return res.send({
                    error: error
                })
            }
        })
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}
exports.create = async (req, res, next) => {
    try {
        let {
            name,
            content
        } = req.body
        if (!name || !content) {
            return res.satus(401).send({
                error: "Name or content cannot be null. Please check this and try again."
            })
        } else {
            Note.create({
                name,
                content,
                belongs_to: req.groupId,
                owner: req.userId
            }, async (error, note) => {
                if (error) {
                    return utils.ServerError(res)
                } else {
                    await GroupControl.addIdNoteInGroup(req.groupId, note._id)
                    return res.send(note)
                }
            })
        }
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}
exports.update = async (req, res, next) => {
    try {
        let {
            name,
            content
        } = req.body
        if (!name || !content) {
            errors.fields_not_found.fields = ['name', 'content']
            return responses.sendError(res, errors.fields_not_found)
            // return res.satus(401).send({
            //     error: "Name or content cannot be null. Please check this and try again."
            // })
        } else {
            Note.findOneAndUpdate({
                _id: req.params.noteId,
                belongs_to: req.groupId
            }, {
                name,
                content,
                belongs_to: req.groupId,
                owner: req.userId
            }, {
                new: true
            }, async (error, note) => {
                if (error) {
                    return responses.sendError(res, errors.note_not_found)
                } else {
                    const query = Note.findOne({
                        _id: req.params.noteId,
                        belongs_to: req.groupId
                    }, (error, note) => {
                        if (error) {
                            return responses.sendError(res, errors.note_not_found)
                        } else {

                            return res.send({
                                name: note.name,
                                content: note.content,
                                belongs_to: {
                                    id: belongs_to.id,
                                    name: belongs_to.name
                                },
                                owner: {
                                    id: owner.id,
                                    name: owner.name,
                                    email: owner.email
                                }
                            })
                        }
                    })
                    query.exec()
                }
            })
        }
    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}
exports.delete = async (req, res, next) => {
    try {
        Note.findOne({
            _id: req.params.noteId,
            belongs_to: req.groupId
        }, async (error, note) => {
            if (note) {
                if (ownerVerify(req.userId, note.id)) {
                    Note.findOneAndRemove({
                        _id: req.params.noteId,
                        belongs_to: req.groupId
                    }, async (error) => {
                        if (!error) {
                            await GroupControl.removeIdNoteInGroup(req.groupId, req.params.noteId)
                            return res.send({
                                msg: "Note delete successfull"
                            })
                        } else {
                            throw error
                        }
                    })
                } else {
                    return responses.sendError(res, errors.not_authorized)
                }

            } else {
                return responses.sendError(res, errors.note_not_found)
            }
        })

    } catch (error) {
        return responses.sendError(res, errors.internal_error)
    }
}



exports.removeMore = async (noteList, groupId) => {
    try {
        await Promise.all(noteList.map(async note => {
            await Note.findOneAndRemove({
                _id: note,
                belongs_to: groupId
            })
        }))
        return true
    } catch (error) {
        return false
    }
}