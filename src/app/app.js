const express = require("express"),
    bp = require("body-parser"),
    app = express(),
    compression = require('compression'),
    favicon = require('serve-favicon'),
    path = require('path'),
    responses = require('./controllers/responses'),
    errors = require('../config/errors.json')
const {
    exec
} = require('child_process')

app.use(bp.urlencoded({
    extended: true
}));
// usando body-parser para tranformar a requisição em objeto json
app.use(bp.json());
app.use(compression())
// app.use(favicon(path.join(__dirname, '../static', 'icon.svg')))

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization "
    );
    res.header('Access-Control-Allow-Methods', 'GET, PATCH, POST, DELETE, OPTIONS');
    res.header("Content-Type", " application/json; charset=utf-8")
    // res.header('Provided-By', 'btm-api')
    res.header('X-Powered-By', 'btm-api')
    if (req.method === "OPTIONS")
        res.send();
    else
        next();
    // next();
});


require("./routes/")(app);



// bloco que trata a resposta quando o usuário não encontra nenhuma rota disponivel
app.use((req, res, next) => {
    const error = new Error("Recurso não encontrado!");
    error.status = 404;
    next(error);
});


app.use(async (error, req, res, next) => {
    let e
    res.status(error.status || 500);
        errors.resource_not_found.message = error.message
    await exec('node -v', (error, stdout, stderr) => {
        console.log({error: error })
        e = stdout
        errors.resource_not_found.teste = e
        return responses.sendError(res, errors.resource_not_found)
     })
    // res.json({
    //     error: error.status + "! " + error.message
    // });
});

module.exports = app;